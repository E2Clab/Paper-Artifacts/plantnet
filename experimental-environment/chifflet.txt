MAIL=/var/mail/USER
LANGUAGE=en_US:en
USER=USER
SSH_CLIENT=172.16.71.6 55608 22
XDG_SESSION_TYPE=tty
SHLVL=0
HOME=/USER
SSH_TTY=/dev/pts/0
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/0/bus
LOGNAME=USER
_=/bin/sh
XDG_SESSION_CLASS=user
TERM=screen
XDG_SESSION_ID=9
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
XDG_RUNTIME_DIR=/run/user/0
LANG=en_US.UTF-8
SHELL=/bin/bash
PWD=/USER
LC_ALL=en_US.UTF-8
SSH_CONNECTION=172.16.71.6 55608 172.16.38.3 22
Distributor ID:	Debian
Description:	Debian GNU/Linux 10 (buster)
Release:	10
Codename:	buster
Linux chifflet-3.lille.grid5000.fr 4.19.0-16-amd64 #1 SMP Debian 4.19.181-1 (2021-03-19) x86_64 GNU/Linux
Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
Address sizes:       46 bits physical, 48 bits virtual
CPU(s):              56
On-line CPU(s) list: 0-55
Thread(s) per core:  2
Core(s) per socket:  14
Socket(s):           2
NUMA node(s):        2
Vendor ID:           GenuineIntel
CPU family:          6
Model:               79
Model name:          Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz
Stepping:            1
CPU MHz:             1223.349
CPU max MHz:         3300.0000
CPU min MHz:         1200.0000
BogoMIPS:            4799.76
Virtualization:      VT-x
L1d cache:           32K
L1i cache:           32K
L2 cache:            256K
L3 cache:            35840K
NUMA node0 CPU(s):   0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54
NUMA node1 CPU(s):   1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55
Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid dca sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb cat_l3 cdp_l3 invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm cqm rdt_a rdseed adx smap intel_pt xsaveopt cqm_llc cqm_occup_llc cqm_mbm_total cqm_mbm_local dtherm ida arat pln pts md_clear flush_l1d
MemTotal:       792528752 kB
MemFree:        790003988 kB
MemAvailable:   787399068 kB
Buffers:           33012 kB
Cached:          1091992 kB
SwapCached:            0 kB
Active:           561908 kB
Inactive:         802336 kB
Active(anon):     240604 kB
Inactive(anon):     9848 kB
Active(file):     321304 kB
Inactive(file):   792488 kB
Unevictable:        5320 kB
Mlocked:            5320 kB
SwapTotal:       3905532 kB
SwapFree:        3905532 kB
Dirty:               276 kB
Writeback:             0 kB
AnonPages:        240504 kB
Mapped:           153548 kB
Shmem:             11352 kB
Slab:             388008 kB
SReclaimable:     137664 kB
SUnreclaim:       250344 kB
KernelStack:       11984 kB
PageTables:         3792 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    400169908 kB
Committed_AS:    2415440 kB
VmallocTotal:   34359738367 kB
VmallocUsed:           0 kB
VmallocChunk:          0 kB
Percpu:            34560 kB
HardwareCorrupted:     0 kB
AnonHugePages:    112640 kB
ShmemHugePages:        0 kB
ShmemPmdMapped:        0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
Hugetlb:               0 kB
DirectMap4k:      516872 kB
DirectMap2M:     6727680 kB
DirectMap1G:    800063488 kB
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 372.6G  0 disk 
├─sda1   8:1    0   3.7G  0 part [SWAP]
├─sda2   8:2    0  19.6G  0 part 
├─sda3   8:3    0  22.4G  0 part /
├─sda4   8:4    0     1K  0 part 
└─sda5   8:5    0   327G  0 part /tmp
sr0     11:0    1  1024M  0 rom  
[0:0:0:0]    disk    TOSHIBA  PX04SMB040       AM07  /dev/sda    400GB
[10:0:0:0]   cd/dvd  HL-DT-ST DVD+-RW GTA0N    A3C0  /dev/sr0        -
Thu Apr  8 19:47:46 2021       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.80.02    Driver Version: 450.80.02    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  GeForce GTX 108...  On   | 00000000:04:00.0 Off |                  N/A |
| 23%   21C    P8     8W / 250W |      1MiB / 11178MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   1  GeForce GTX 108...  On   | 00000000:82:00.0 Off |                  N/A |
| 23%   19C    P8     8W / 250W |      1MiB / 11178MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
H/W path              Device      Class          Description
============================================================
                                  system         PowerEdge R730 (SKU=NotProvided;ModelName=PowerEdge R730)
/0                                bus            0WCJNT
/0/0                              memory         64KiB BIOS
/0/400                            processor      Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz
/0/400/700                        memory         896KiB L1 cache
/0/400/701                        memory         3584KiB L2 cache
/0/400/702                        memory         35MiB L3 cache
/0/401                            processor      Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz
/0/401/703                        memory         896KiB L1 cache
/0/401/704                        memory         3584KiB L2 cache
/0/401/705                        memory         35MiB L3 cache
/0/1000                           memory         768GiB System Memory
/0/1000/0                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/1                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/2                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/3                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/4                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/5                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/6                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/7                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/8                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/9                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/a                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/b                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/c                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/d                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/e                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/f                         memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/10                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/11                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/12                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/13                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/14                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/15                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/16                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/1000/17                        memory         32GiB DIMM DDR4 Synchronous Registered (Buffered) 2400 MHz (0.4 ns)
/0/100                            bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DMI2
/0/100/1                          bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 1
/0/100/1/0            scsi0       storage        MegaRAID SAS-3 3108 [Invader]
/0/100/1/0/0.0.0      /dev/sda    disk           400GB PX04SMB040
/0/100/1/0/0.0.0/1    /dev/sda1   volume         3814MiB Linux swap volume
/0/100/1/0/0.0.0/2    /dev/sda2   volume         19GiB EXT4 volume
/0/100/1/0/0.0.0/3    /dev/sda3   volume         22GiB EXT4 volume
/0/100/1/0/0.0.0/4    /dev/sda4   volume         326GiB Extended partition
/0/100/1/0/0.0.0/4/5  /dev/sda5   volume         326GiB EXT4 volume
/0/100/2                          bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 2
/0/100/2/0                        display        NVIDIA Corporation
/0/100/2/0.1                      multimedia     NVIDIA Corporation
/0/100/2.2                        bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 2
/0/100/3                          bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 3
/0/100/3/0            eno1        network        82599ES 10-Gigabit SFI/SFP+ Network Connection
/0/100/3/0.1          eno2        network        82599ES 10-Gigabit SFI/SFP+ Network Connection
/0/100/3.2                        bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 3
/0/100/5                          generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Map/VTd_Misc/System Management
/0/100/5.1                        generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D IIO Hot Plug
/0/100/5.2                        generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D IIO RAS/Control Status/Global Errors
/0/100/5.4                        generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D I/O APIC
/0/100/11                         generic        C610/X99 series chipset SPSR
/0/100/11.4                       storage        C610/X99 series chipset sSATA Controller [AHCI mode]
/0/100/16                         communication  C610/X99 series chipset MEI Controller #1
/0/100/16.1                       communication  C610/X99 series chipset MEI Controller #2
/0/100/1a                         bus            C610/X99 series chipset USB Enhanced Host Controller #2
/0/100/1a/1           usb1        bus            EHCI Host Controller
/0/100/1a/1/1                     bus            USB hub
/0/100/1a/1/1/6                   bus            Gadget USB HUB
/0/100/1c                         bridge         C610/X99 series chipset PCI Express Root Port #1
/0/100/1c.4                       bridge         C610/X99 series chipset PCI Express Root Port #5
/0/100/1c.4/0         eno3        network        I350 Gigabit Network Connection
/0/100/1c.4/0.1       eno4        network        I350 Gigabit Network Connection
/0/100/1c.7                       bridge         C610/X99 series chipset PCI Express Root Port #8
/0/100/1c.7/0                     bridge         SH7758 PCIe Switch [PS]
/0/100/1c.7/0/0                   bridge         SH7758 PCIe Switch [PS]
/0/100/1c.7/0/0/0                 bridge         SH7758 PCIe-PCI Bridge [PPB]
/0/100/1c.7/0/0/0/0               display        G200eR2
/0/100/1d                         bus            C610/X99 series chipset USB Enhanced Host Controller #1
/0/100/1d/1           usb2        bus            EHCI Host Controller
/0/100/1d/1/1                     bus            USB hub
/0/100/1f                         bridge         C610/X99 series chipset LPC Controller
/0/100/1f.2           scsi10      storage        C610/X99 series chipset 6-Port SATA Controller [AHCI mode]
/0/100/1f.2/0.0.0     /dev/cdrom  disk           DVD+-RW GTA0N
/0/4                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 0
/0/6                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 0
/0/7                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 0
/0/8                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 1
/0/9                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 1
/0/a                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 1
/0/b                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link 0/1
/0/c                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link 0/1
/0/d                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link 0/1
/0/e                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link Debug
/0/f                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/10                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/11                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/12                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/13                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/14                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/15                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/16                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/17                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/18                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/19                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/1a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/1b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/1c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/1d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/1e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/1f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/20                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/21                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/22                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/23                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/24                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R2PCIe Agent
/0/25                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R2PCIe Agent
/0/26                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Ubox
/0/27                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Ubox
/0/28                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Ubox
/0/29                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 0
/0/2a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 0
/0/2b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 0 Debug
/0/2c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 1
/0/2d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 1
/0/2e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 1 Debug
/0/2f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Target Address/Thermal/RAS
/0/30                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Target Address/Thermal/RAS
/0/31                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel Target Address Decoder
/0/32                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel Target Address Decoder
/0/33                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Broadcast
/0/34                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Global Broadcast
/0/35                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 0 Thermal Control
/0/36                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 1 Thermal Control
/0/37                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 0 Error
/0/38                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 1 Error
/0/39                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/3a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/3b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/3c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/3d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Target Address/Thermal/RAS
/0/3e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Target Address/Thermal/RAS
/0/3f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Channel Target Address Decoder
/0/40                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Channel Target Address Decoder
/0/41                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Broadcast
/0/42                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Global Broadcast
/0/43                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 0 Thermal Control
/0/44                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 1 Thermal Control
/0/45                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 0 Error
/0/46                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 1 Error
/0/47                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/48                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/49                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/4a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/4b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/4c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/4d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/4e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/4f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/50                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/51                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/1                              bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 1
/0/2                              bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 2
/0/2/0                            display        NVIDIA Corporation
/0/2/0.1                          multimedia     NVIDIA Corporation
/0/3                              bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 3
/0/3.2                            bridge         Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D PCI Express Root Port 3
/0/5                              generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Map/VTd_Misc/System Management
/0/5.1                            generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D IIO Hot Plug
/0/5.2                            generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D IIO RAS/Control Status/Global Errors
/0/5.4                            generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D I/O APIC
/0/52                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 0
/0/53                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 0
/0/54                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 0
/0/55                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 1
/0/56                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 1
/0/57                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D QPI Link 1
/0/58                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link 0/1
/0/59                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link 0/1
/0/5a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link 0/1
/0/5b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R3 QPI Link Debug
/0/5c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/5d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/5e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/5f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/60                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/61                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/62                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/63                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/64                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/65                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/66                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/67                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/68                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/69                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/6a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/6b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/6c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/6d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/6e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/6f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/70                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Caching Agent
/0/71                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R2PCIe Agent
/0/72                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D R2PCIe Agent
/0/73                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Ubox
/0/74                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Ubox
/0/75                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Ubox
/0/76                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 0
/0/77                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 0
/0/78                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 0 Debug
/0/79                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 1
/0/7a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 1
/0/7b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Home Agent 1 Debug
/0/7c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Target Address/Thermal/RAS
/0/7d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Target Address/Thermal/RAS
/0/7e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel Target Address Decoder
/0/7f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel Target Address Decoder
/0/80                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Broadcast
/0/81                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Global Broadcast
/0/82                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 0 Thermal Control
/0/83                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 1 Thermal Control
/0/84                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 0 Error
/0/85                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 0 - Channel 1 Error
/0/86                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/87                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/88                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/89                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 0/1 Interface
/0/8a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Target Address/Thermal/RAS
/0/8b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Target Address/Thermal/RAS
/0/8c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Channel Target Address Decoder
/0/8d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Channel Target Address Decoder
/0/8e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Broadcast
/0/8f                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Global Broadcast
/0/90                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 0 Thermal Control
/0/91                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 1 Thermal Control
/0/92                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 0 Error
/0/93                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Memory Controller 1 - Channel 1 Error
/0/94                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/95                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/96                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/97                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D DDRIO Channel 2/3 Interface
/0/98                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/99                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/9a                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/9b                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/9c                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/9d                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/9e                             generic        Xeon E7 v4/Xeon E5 v4/Xeon E3 v4/Xeon D Power Control Unit
/0/9f                             system         PnP device PNP0b00
/0/a0                             system         PnP device PNP0c02
/0/a1                             communication  PnP device PNP0501
/0/a2                             communication  PnP device PNP0501
/0/a3                             generic        PnP device IPI0001
/1                                power          09TMRFA00
/2                                power          09TMRFA00
/3                    docker0     network        Ethernet interface
