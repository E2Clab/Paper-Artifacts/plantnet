#!/bin/bash
# It collects statistics from identification engine and crash

echo "Repeat: $1"
echo "Interval: $2"
echo "Server: $3"
echo "Dir: $4"

mkdir -p $4
cd $4 && rm -f *
file_rtt="$4/identify_status_rtt.log"
file_wget="$4/identify_status_wget.log"

echo $file_rtt
echo $file_wget

for ((i=1;i<=$1;i++));
do
    echo "----- request $i -----" >> $file_rtt
    date +"%T.%3N" >> $file_rtt
    wget -P $4 -a $file_wget $3
    date +"%T.%3N" >> $file_rtt
    sleep $2
done
